var jwt = require('jwt-simple');
var pg = require('pg');
var connectionString = 'postgres://postgres@localhost:5432/postgres';

var admin = {
  getAuthors : function(req, res) {
    var authors = [];
    // retrieve all authors that are linked to certain admin author
    pg.connect(connectionString, function(err, client, done) {
      if (err) {
        done();
        console.log(err);
        res.json({
          "status": "error",
          "message": "Couldn't connect to the database"
        });
      } else {
        console.log(req.body.company_id);
        var query = client.query("SELECT author_id, author_firstname, author_lastname, username FROM authors");
        // var author (?)
        query.on('row', function(row) {
          author = {id: "", firstname: "", lastname: "", username: ""};
          author.id = row.author_id;
          author.firstname = row.author_firstname;
          author.lastname = row.author_lastname;
          author.username = row.username;
          authors.push(author);
        });

        query.on('end', function() {
          done();
          res.json(authors);
        });
      }
    });
  }/*,
  createAuthor : function(req, res) {
    // create one author
    pg.connect(connectionString, function(err, client, done) {
      if (err) {
        done();
        console.log(err);
        res.json([]);
        return;
      } else {
        var query = client.query("SELECT author_id, author_firstname, author_lastname, username FROM authors WHERE username = $1", [username]);
        // var author (?)
        query.on('row', function(row) {
          author = {id: "", firstname: "", lastname: "", username: ""};
          author.id = row.author_id;
          author.firstname = row.author_firstname;
          author.lastname = row.author_lastname;
          author.username = row.username;
          authors.push(author);
        });

        query.on('end', function() {
          done();
        });
      }
      res.json(author);
      return;
    });
  }/*,
  deleteAuthor : function(req, res) {
  }  */
};
module.exports = admin;
