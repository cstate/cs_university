var jwt = require('jwt-simple');
var pg = require('pg');
var psHash = require('password-hash');
var connectionString = 'postgres://postgres@localhost:5432/postgres';

var auth = {
  login: function(req, res) {

    var username = req.body.username || '';
    var password = req.body.password || '';

    if (username == '' || password == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
      return;
    }

    // Send a query to the database and check if credentials are valid
    // var dbUserObj = auth.validate(username, password);
    pg.connect(connectionString, function(err, client, done) {

      var stat = 401;
      var message = '';
      // Handle Connection errors
      if (err) {
        done();
        console.log(err);
        stat = 401;
      } else {
        console.log("auth.js [validate]: everything seems fine");
        var query = client.query("SELECT author_firstname, comp_owner, salt, password FROM authors WHERE username = $1::varchar", [username]);
        var user_data = {"name":'', "admin":'', "username":''}
        query.on('row', function(row) {
          console.log("auth.js [validate]: found data!");
          var salt = row.salt;
          var hashed_password = row.password;
          if (psHash.verify(password + salt, hashed_password)) {
            console.log("Success!");
            stat = 200;
            message = "Logged!";

            user_data.name = row.author_firstname;
            user_data.admin = row.comp_owner;
            user_data.username = username;
          } else {
            console.log("Neehh!");
            stat = 410;
            message = "Invalid credentials";
          }
        });

        query.on('end', function() {
          done();
          res.status(stat);
          if (stat != 200) {
            res.json({
              "status": stat,
              "message": message
            });
          } else {
            res.json(getToken(user_data));
          }
        });
      }

    });
    return;
  },
  validateUser: function(username, callback) {
    pg.connect(connectionString, function(err, client, done) {
      if (err) {
        done();
        console.log(err);
        // return false;
        callback();
      } else {
        var r = {"role": '', "companyid": -1};
        var query = client.query("SELECT company_id, comp_owner from authors WHERE username = $1", [username]);
        query.on('row', function(row) {
          if (row.comp_owner) {
            r.role = "admin";
            r.companyid = row.company_id;
          } else {
            r.role = "worker";
            r.companyid = row.company_id;
          }
        });
        query.on('end', function() {
          done();
          console.log("Calling callback function with r: " + r.role);
          callback(r);
        });
      }
    });
  }
}

function getToken(user) {
  var expires = expiresIn(7);
  var token = jwt.encode({
    exp: expires
  }, require('../config/secret')());

  return {
    token: token,
    expires: expires,
    user: user
  };
}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
