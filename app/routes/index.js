var express = require('express');
var router = express.Router();

var auth = require('./auth.js');
// var courses = require('./courses.js');
var admin = require('./admin.js');

/*
 * Routes that can be accessed by any one
 */
router.post('/login', auth.login);

/*
 * Router that can be accessed only by authenticated users
 */
// router.get('/api/v1/courses', courses.getAll);

/*
 * Routes that can be accessed only by authenticaed users & authorized users
 */
router.get('/api/v1/admin/authors', admin.getAuthors);
//router.post('/api/v1/admin/author/', admin.createAuthor);
//router.delete('/api/v1/admin/author/:id', admin.deleteAuthor);

module.exports = router;
