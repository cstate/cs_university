var contCreator = angular.module('contCreator', []);

var course_data = {
  "course": {
    "title" : "Course Title",
    "subtitle" : "Fancy Subtitle",
    "description" : "sdfsdf"
  },
  "topics": [
    {'title': 'Topic #1'},
    {'title': 'Topic #2'}
  ]
};

contCreator.controller('basicCourseCtrl', function($scope) {
  $scope.course = course_data.course;
  $scope.topics = course_data.topics;
  $scope.changeTitle = function () {
    course_data.course.title = "Juas juas";
  };
});

contCreator.directive('contentEditable', function() {
  return {
    require: 'ngModel', 
    link: function(scope, elm, attrs, ctrl) {
      elm.on('blur', function() {
        ctrl.$setViewValue(elm.html());
      });
    }
  };
});
