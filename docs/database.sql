
CREATE SEQUENCE students_student_id_seq;

CREATE TABLE students (
                student_id INTEGER NOT NULL DEFAULT nextval('students_student_id_seq'),
                student_firstname VARCHAR NOT NULL,
                student_lastname VARCHAR NOT NULL,
                username VARCHAR NOT NULL,
                password VARCHAR NOT NULL,
                salt VARCHAR NOT NULL,
                email VARCHAR NOT NULL,
                CONSTRAINT students_pk PRIMARY KEY (student_id)
);


ALTER SEQUENCE students_student_id_seq OWNED BY students.student_id;

CREATE SEQUENCE company_company_seq;

CREATE TABLE company (
                company_id INTEGER NOT NULL DEFAULT nextval('company_company_seq'),
                comp_name VARCHAR NOT NULL,
                CONSTRAINT company_pk PRIMARY KEY (company_id)
);


ALTER SEQUENCE company_company_seq OWNED BY company.company_id;

CREATE SEQUENCE courses_course_id_seq;

CREATE TABLE courses (
                course_Id INTEGER NOT NULL DEFAULT nextval('courses_course_id_seq'),
                course_title VARCHAR NOT NULL,
                course_subtitle VARCHAR NOT NULL,
                price REAL NOT NULL,
                published BOOLEAN DEFAULT FALSE NOT NULL,
                company_id INTEGER NOT NULL,
                in_development BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT courses_pk PRIMARY KEY (course_Id)
);


ALTER SEQUENCE courses_course_id_seq OWNED BY courses.course_Id;

CREATE SEQUENCE labs_lab_id_seq_1;

CREATE TABLE labs (
                lab_id INTEGER NOT NULL DEFAULT nextval('labs_lab_id_seq_1'),
                course_Id INTEGER NOT NULL,
                lab_doc VARCHAR NOT NULL,
                script_file VARCHAR NOT NULL,
                CONSTRAINT labs_pk PRIMARY KEY (lab_id)
);


ALTER SEQUENCE labs_lab_id_seq_1 OWNED BY labs.lab_id;

CREATE TABLE student_course_rel (
                student_id INTEGER NOT NULL,
                course_Id INTEGER NOT NULL,
                trial BOOLEAN DEFAULT TRUE NOT NULL,
                finished BOOLEAN DEFAULT FALSE NOT NULL,
                certificate_file VARCHAR,
                current_grade INTEGER DEFAULT 0 NOT NULL,
                progress INTEGER DEFAULT 0 NOT NULL
);


CREATE SEQUENCE topics_topic_id_seq;

CREATE TABLE topics (
                topic_id INTEGER NOT NULL DEFAULT nextval('topics_topic_id_seq'),
                course_Id INTEGER NOT NULL,
                topic_doc VARCHAR NOT NULL,
                CONSTRAINT topics_pk PRIMARY KEY (topic_id)
);


ALTER SEQUENCE topics_topic_id_seq OWNED BY topics.topic_id;

CREATE SEQUENCE subtopics_subtopic_id_seq;

CREATE TABLE subtopics (
                subtopic_id INTEGER NOT NULL DEFAULT nextval('subtopics_subtopic_id_seq'),
                subtopic_doc VARCHAR NOT NULL,
                topic_id INTEGER NOT NULL,
                CONSTRAINT subtopics_pk PRIMARY KEY (subtopic_id)
);


ALTER SEQUENCE subtopics_subtopic_id_seq OWNED BY subtopics.subtopic_id;

CREATE SEQUENCE questions_question_id_seq;

CREATE TABLE questions (
                question_id INTEGER NOT NULL DEFAULT nextval('questions_question_id_seq'),
                question VARCHAR NOT NULL,
                correct_answer VARCHAR NOT NULL,
                incorrect_answers JSON NOT NULL,
                course_Id INTEGER DEFAULT -1,
                topic_id INTEGER DEFAULT -1,
                subtopic_id INTEGER DEFAULT -1,
                lab_id INTEGER DEFAULT -1,
                CONSTRAINT questions_pk PRIMARY KEY (question_id)
);


ALTER SEQUENCE questions_question_id_seq OWNED BY questions.question_id;

CREATE TABLE evaluation (
                question_id INTEGER NOT NULL,
                student_id INTEGER NOT NULL,
                points INTEGER DEFAULT 0 NOT NULL,
                CONSTRAINT evaluation_pk PRIMARY KEY (question_id, student_id)
);


CREATE TABLE authors (
                author_id INTEGER NOT NULL,
                company_id INTEGER NOT NULL,
                comp_owner BOOLEAN DEFAULT FALSE NOT NULL,
                author_firstname VARCHAR NOT NULL,
                author_lastname VARCHAR NOT NULL,
                username VARCHAR NOT NULL,
                password VARCHAR NOT NULL,
                salt VARCHAR NOT NULL,
                email VARCHAR NOT NULL,
                CONSTRAINT authors_pk PRIMARY KEY (author_id)
);


CREATE SEQUENCE fixes_fix_id_seq;

CREATE TABLE fixes (
                fix_id INTEGER NOT NULL DEFAULT nextval('fixes_fix_id_seq'),
                author_id INTEGER NOT NULL,
                course_Id INTEGER NOT NULL,
                topic_id INTEGER,
                subtopic_id INTEGER,
                comment VARCHAR NOT NULL,
                fixed BOOLEAN DEFAULT FALSE NOT NULL,
                pos_x INTEGER NOT NULL,
                pos_y INTEGER NOT NULL,
                CONSTRAINT fixes_pk PRIMARY KEY (fix_id)
);


ALTER SEQUENCE fixes_fix_id_seq OWNED BY fixes.fix_id;

CREATE TABLE validation (
                author_id INTEGER NOT NULL,
                course_Id INTEGER NOT NULL,
                topic_id INTEGER,
                subtopic_id INTEGER,
                validated BOOLEAN DEFAULT FALSE NOT NULL
);


CREATE TABLE course_author_rel (
                author_id INTEGER NOT NULL,
                course_Id INTEGER NOT NULL
);


ALTER TABLE student_course_rel ADD CONSTRAINT students_student_course_rel_fk
FOREIGN KEY (student_id)
REFERENCES students (student_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE evaluation ADD CONSTRAINT students_evaluation_fk
FOREIGN KEY (student_id)
REFERENCES students (student_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE authors ADD CONSTRAINT company_authors_fk
FOREIGN KEY (company_id)
REFERENCES company (company_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE courses ADD CONSTRAINT company_courses_fk
FOREIGN KEY (company_id)
REFERENCES company (company_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE topics ADD CONSTRAINT courses_topics_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE course_author_rel ADD CONSTRAINT courses_course_author_rel_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE student_course_rel ADD CONSTRAINT courses_student_course_rel_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE questions ADD CONSTRAINT courses_questions_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE labs ADD CONSTRAINT courses_labs_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE validation ADD CONSTRAINT courses_validation_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE fixes ADD CONSTRAINT courses_fixes_fk
FOREIGN KEY (course_Id)
REFERENCES courses (course_Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE questions ADD CONSTRAINT labs_questions_fk
FOREIGN KEY (lab_id)
REFERENCES labs (lab_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE subtopics ADD CONSTRAINT topics_subtopics_fk
FOREIGN KEY (topic_id)
REFERENCES topics (topic_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE questions ADD CONSTRAINT topics_questions_fk
FOREIGN KEY (topic_id)
REFERENCES topics (topic_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE questions ADD CONSTRAINT subtopics_questions_fk
FOREIGN KEY (subtopic_id)
REFERENCES subtopics (subtopic_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE evaluation ADD CONSTRAINT questions_evaluation_fk
FOREIGN KEY (question_id)
REFERENCES questions (question_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE course_author_rel ADD CONSTRAINT authors_course_author_rel_fk
FOREIGN KEY (author_id)
REFERENCES authors (author_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE validation ADD CONSTRAINT authors_validation_fk
FOREIGN KEY (author_id)
REFERENCES authors (author_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE fixes ADD CONSTRAINT authors_fixes_fk
FOREIGN KEY (author_id)
REFERENCES authors (author_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
